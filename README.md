# FTB-Docker-server
Trying out an idea for deploying an FTB server via Docker. 

INSTALLATION:

1. Install docker: (http://docs.docker.com/engine/installation/)
    curl -sSL https://get.docker.com/ | sh

2. Install docker-compose: (https://docs.docker.com/compose/install/)
    curl -L https://github.com/docker/compose/releases/download/1.5.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose

3. Pull down GitHub repo: (https://github.com/davidzlewis/FTB-Docker-server/releases/latest)
    wget https://github.com/davidzlewis/FTB-Docker-server/archive/v1.0.0.zip

4. Unzip the files:
    unzip v1.0.0.zip

5. Create the persistent data volume: 
    docker volume create --name=FTB-data

6. Run docker-compose:
    cd FTB-docker-server && docker-compose up


I moved this image to the Docker Hub!

docker pull davidzlewis/ftb-docker-server

DONT FORGET TO SET THE DEFAULT OP! 

